//
// Created by mashusik on 13.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_MALLOC_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_MALLOC_H

#include "mem.h"
#include "mem_internals.h"
static inline struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void test_all();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_MALLOC_H
