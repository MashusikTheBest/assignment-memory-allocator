
#include "test_malloc.h"
#include "util.h"
static void test1() {
    fputs("test 1 started\n", stdout);
    void *malloc1 = _malloc(100);
    void *malloc2 = _malloc(200);
    void *malloc3 = _malloc(500);
    struct block_header *block1 = block_get_header(malloc1);
    struct block_header *block2 = block_get_header(malloc2);
    struct block_header *block3 = block_get_header(malloc3);
    debug_heap(stdout, block1);
    bool check1 = !block1->is_free;
    bool check2 = !block2->is_free;
    bool check3 = !block3->is_free;
    if (check1 && check2 && check3) {
        printf("test1 ok\n");
        _free(malloc1);
        _free(malloc2);
        _free(malloc3);
        debug_heap(stdout, block1);
    } else err("test1 fail\n");
}


static void test2(){
     fputs("test 2 started\n", stdout);
     void *malloc1 = _malloc(100);
     void *malloc2 = _malloc(200);
     void *malloc3 = _malloc(500);
     struct block_header *block1 = block_get_header(malloc1);
     struct block_header *block2 = block_get_header(malloc2);
     struct block_header *block3 = block_get_header(malloc3);
     debug_heap(stdout, block1);
     _free(malloc1);
     debug_heap(stdout, block1);
     bool check1 = block1->is_free;
     bool check2 = !block2->is_free;
     bool check3 = !block3->is_free;
     if (check1 && check2 && check3) {
         printf("test2 ok\n");
         _free(malloc2);
         _free(malloc3);
         debug_heap(stdout, block1);
     } else err("test2 fail\n");
}

static void test3(){
    fputs("test 3 started\n", stdout);
    void *malloc1 = _malloc(100);
    void *malloc2 = _malloc(200);
    void *malloc3 = _malloc(500);
    struct block_header *block1 = block_get_header(malloc1);
    struct block_header *block2 = block_get_header(malloc2);
    struct block_header *block3 = block_get_header(malloc3);
    debug_heap(stdout, block1);
    _free(malloc1);
    debug_heap(stdout, block1);
    _free(malloc2);
    debug_heap(stdout, block1);
    bool check1 = block1->is_free;
    bool check2 = block2->is_free;
    bool check3 = !block3->is_free;
    if (check1 && check2 && check3) {
        printf("test3 ok\n");
        _free(malloc3);
        debug_heap(stdout, block1);
    } else err("test3 fail\n");
}
static void test4(){
    fputs("test 4 started\n", stdout);
    void *malloc1 = _malloc(7000);
    void *malloc2 = _malloc(4000);
    struct block_header *block1 = block_get_header(malloc1);
    struct block_header *block2 = block_get_header(malloc2);
    void*  tmp = block1->contents + block1->capacity.bytes;
    debug_heap(stdout, block1);
    bool check1 = !block1->is_free;
    bool check2 = !block2->is_free;
    bool check3 = block2 == tmp ;
    if (check1 && check2 && check3) {
        printf("test4 ok\n");
        _free(malloc1);
        _free(malloc2);
        debug_heap(stdout, block1);
    }else err("test4 fail\n");
}
static void test5() {
    fputs("test 5 started\n", stdout);
    void *malloc1 = _malloc(12000);
    struct block_header *block1 = block_get_header(malloc1);
    debug_heap(stdout, block1);
    struct block_header* last_block = block1;
    while(last_block->next){
        last_block = last_block->next;
    }
    void*  tmp = last_block->contents + last_block->capacity.bytes;
    mmap(tmp, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void *malloc2 = _malloc(7000);
    debug_heap(stdout, block1);
    struct block_header *block2 = block_get_header(malloc2);
    bool check1 = !block1->is_free;
    bool check2 = !block2->is_free;
    bool check3 = tmp != block2;
    if (check1 && check2 && check3) {
        printf("test5 ok\n");
        _free(malloc1);
        _free(malloc2);
        debug_heap(stdout, block1);
    }else err("test5 fail\n");
}

void test_all() {
    heap_init(800);
    test1();
    test2();
    test3();
    test4();
    test5();
}